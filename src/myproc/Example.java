package myproc;

public class Example {

  @Param(input = true, type = Param.ParameterType.INT)
  int ulaz1;

  @Param(input = true, type = Param.ParameterType.INT)
  int ulaz2;

  public Example(int ulaz1, int ulaz2) {
    this.ulaz1 = ulaz1;
    this.ulaz2 = ulaz2;
  }
}
