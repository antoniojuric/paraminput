package myproc;

import com.google.auto.service.AutoService;
import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.TypeSpec;

import javax.annotation.processing.*;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.tools.Diagnostic;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

@SupportedSourceVersion(SourceVersion.RELEASE_8)
@SupportedAnnotationTypes({"myproc.Param"})
@AutoService(MyProcessor.class)
public class MyProcessor extends AbstractProcessor {

  private Messager messager;
  private Filer filer;
  boolean firstRun = true;

  @Override
  public synchronized void init(ProcessingEnvironment processingEnv) {
    super.init(processingEnv);
    messager = processingEnv.getMessager();
    filer = processingEnv.getFiler();
  }

  @Override
  public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
    if (!firstRun) { return true; }
    firstRun = false;
    List<ParamAnnotatedField> annotatedElements = new LinkedList<>();
    for (Element element : roundEnv.getElementsAnnotatedWith(Param.class)) {

      // can only be field or local variable
      if (element.getKind() != ElementKind.FIELD && element.getKind() != ElementKind.LOCAL_VARIABLE) {
        error(element, "Only fields can be annotated with @%s.", Param.class.getSimpleName());
        return true;
      }

      System.out.println("oznaceni: " + element.getSimpleName());

      annotatedElements.add(new ParamAnnotatedField(element));
      // TODO(ajuric): parsiranje, vidjet jel treba jos sta u paramannotatedfield
    }

    try {
      generateCode(annotatedElements);
    } catch (IOException e) {
      //error();
      //TODO(ajuric): sta sad?
    }

    return true;
  }

  private void generateCode(List<ParamAnnotatedField> annotatedElements) throws IOException {
    MethodSpec drawInput = MethodSpec.methodBuilder("drawInput")
      .addModifiers(Modifier.PRIVATE)
      .addParameter(List.class, "params")
      .returns(void.class)
      .addCode(" setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);\n" +
        "    setLocation(200, 200);\n" +
        "    setTitle(\"Input parameters\");\n" +
        "    //setSize(500, 300);\n" +
        "\n" +
        "    Font font = new Font(Font.MONOSPACED, Font.PLAIN, 30);\n" +
        "\n" +
        "    setLayout(new GridLayout(0, 2));\n" +
        "    for (String param : params) {\n" +
        "      JLabel label = new JLabel(\"@\" + param + \":\");\n" +
        "      label.setHorizontalAlignment(SwingConstants.TRAILING);\n" +
        "      label.setFont(font);\n" +
        "      add(label); // left\n" +
        "\n" +
        "      JTextField field = new JTextField();\n" +
        "      field.setHorizontalAlignment(SwingConstants.LEADING);\n" +
        "      Border rounded = new LineBorder(new Color(210,210,210), 1, true);\n" +
        "      Border empty = new EmptyBorder(0, 5, 0, 0);\n" +
        "      field.setBorder(rounded);\n" +
        "      Border border = new CompoundBorder(rounded, empty);\n" +
        "      //field.setBorder(new LineBorder(Color.black, 1));\n" +
        "      field.setBorder(border);\n" +
        "      field.setFont(font);\n" +
        "      add(field); // right\n" +
        "    }\n" +
        "\n" +
        "    JButton button = new JButton(\"Run\");\n" +
        "    button.setFont(font);\n" +
        "    add(button);").build();

    StringBuilder params = new StringBuilder();
    for (ParamAnnotatedField field : annotatedElements) {
      System.out.println("params.add(\"" + field.getParameterName() + "\");\n");
      params.append("params.add(\"" + field.getParameterName() + "\");\n");
    }


    System.out.println("ovo je params: " + params);

    MethodSpec main = MethodSpec.methodBuilder("main")
      .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
      .returns(void.class)
      .addParameter(String[].class, "args")
      .addException(InvocationTargetException.class)
      .addException(InterruptedException.class)
      .addCode(" List<String> params = new LinkedList<>();\n" +
        params.toString() +
        " SwingUtilities.invokeAndWait(new Runnable() {\n" +
        "      @Override\n" +
        "      public void run() {\n" +
        "        JFrame frame = new ParameterInputFrame(params);\n" +
        "        frame.setVisible(true);\n" +
        "        frame.pack();\n" +
        "      }\n" +
        "    });").build();

    MethodSpec constructor = MethodSpec.constructorBuilder()
      .addParameter(List.class, "params")
      .addStatement("drawInput(params);")
      .build();

    TypeSpec parameterInputFrame = TypeSpec.classBuilder("ParameterInputFrame")
      .addModifiers(Modifier.PUBLIC)
      .addMethod(drawInput)
      .addMethod(main)
      .addMethod(constructor)
      .build();

    JavaFile
      .builder("", parameterInputFrame) // TODO(ajuric): this package is not good ...
      .build()
      .writeTo(new File("C:\\tmp"));
  }

  private void error(Element e, String msg, Object... args) {
    messager.printMessage(
      Diagnostic.Kind.ERROR,
      String.format(msg, args),
      e);
  }
}
