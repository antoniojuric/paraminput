package myproc;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.LOCAL_VARIABLE})
public @interface Param {

  public enum ParameterType {
    INT, FLOAT, DOUBLE, STRING,
    INT_ARRAY, FLOAT_ARRAY, DOUBLE_ARRAY, STRING_ARRAY,
    LIST
  }

  public boolean input() default true;

  public ParameterType type();
}
