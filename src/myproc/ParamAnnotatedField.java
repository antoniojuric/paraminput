package myproc;

import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;

public class ParamAnnotatedField {

  private TypeElement annotatedClassElement;
  private String parameterName;
  private Param.ParameterType parameterType;

  public ParamAnnotatedField(Element element) {
    Param annotation = element.getAnnotation(Param.class);
    parameterName = element.getSimpleName().toString();
    parameterType = annotation.type();
  }

  public String getParameterName() {
    return parameterName;
  }
}
