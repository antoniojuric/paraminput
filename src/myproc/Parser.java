package myproc;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.LinkedList;
import java.util.List;

public class Parser extends JFrame {

  public static void main(String[] args) throws InvocationTargetException, InterruptedException {

    List<String> params = new LinkedList<>();

    Class<Example> obj = Example.class;
    for (Field field : obj.getDeclaredFields()) {
      if (field.isAnnotationPresent(Param.class)) {
        params.add(field.getName());
      }
    }

    SwingUtilities.invokeAndWait(new Runnable() {
      @Override
      public void run() {
        JFrame frame = new Parser(params);
        frame.setVisible(true);
        frame.pack();
      }
    });
  }

  public Parser(List<String> params) {
    setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    setLocation(200, 200);
    setTitle("Input parameters");
    //setSize(500, 300);

    Font font = new Font(Font.MONOSPACED, Font.PLAIN, 30);

    setLayout(new GridLayout(0, 2));
    for (String param : params) {
      JLabel label = new JLabel("@" + param + ":");
      label.setHorizontalAlignment(SwingConstants.TRAILING);
      label.setFont(font);
      add(label); // left

      JTextField field = new JTextField();
      field.setHorizontalAlignment(SwingConstants.LEADING);
      Border rounded = new LineBorder(new Color(210,210,210), 1, true);
      Border empty = new EmptyBorder(0, 5, 0, 0);
      field.setBorder(rounded);
      Border border = new CompoundBorder(rounded, empty);
      //field.setBorder(new LineBorder(Color.black, 1));
      field.setBorder(border);
      field.setFont(font);
      add(field); // right
    }

    JButton button = new JButton("Run");
    button.setFont(font);
    add(button);
  }
}
